import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, Image, Button, TouchableOpacity} from 'react-native';
import {StackNavigator} from 'react-navigation';


//#27ae60
//<Image source={require('./img/gotcha.jpg')} style={{width: 293, height: 160}}/>

onPress = () => {
    this.setState({
        count: this.state.count + 10
    })
}

class App extends React.Component {
    static navigationOptions =
    {
        title: 'Home',
    };

    FunctionToOpenSecondActivity = () => {
        this.props.navigation.navigate('Second');

    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={[styles.text_white, styles.h1, styles.boldText]}>Hey!</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_white, styles.h4, styles.boldText]}>Welcome to COOKER</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_white, styles.h4, styles.boldText]}>Let's find out great food{"\n"} for you to
                    cook</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <TouchableOpacity
                    style={styles.btnMainPage}
                    onPress={this.FunctionToOpenSecondActivity}
                >
                    <Text style={styles.btnTextStyle}>LET'S START</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

class SecondActivity extends Component
{
    static navigationOptions =
        {
            title: 'Choose Food Category',
        };

    FunctionToOpenSecondActivity = () => {
        this.props.navigation.navigate('Third');

    };

    render()
    {
        return(
            <View style={styles.container2}>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_white, styles.h3, styles.boldText, styles.TextCenter]}>What categories
                    <Text>{"\n"}</Text>
                    do you like?</Text>
                <Text>{"\n"}</Text>
                <View style={{backgroundColor: 'white', flex: 0.8, width: '99%', borderRadius: 10, padding: 10}}>
                    <Text style={{color:'#f2994a', fontSize: 30, textAlign: 'center', fontWeight: 'bold', paddingTop: 10, paddingBottom: 20}}>UZBEK</Text>
                    <Image source={require('./img/PLOV.jpg')} style={{width: '100%', height: '50%', borderRadius: 10}}/>
                    <Text>{"\n"}</Text>
                    <View>
                        <TouchableOpacity
                            style={{position: 'absolute', left: 0, paddingLeft: 10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/NO.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{position: 'absolute', right: 0, paddingRight:10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/YES.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

class ThirdActivity extends Component {
    static navigationOptions =
        {
            title: 'Choose Food Category',
        };
    FunctionToOpenSecondActivity = () => {
        this.props.navigation.navigate('Fourth');

    };

    render()
    {
        return(
            <View style={styles.container2}>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_white, styles.h3, styles.boldText, styles.TextCenter]}>What categories
                    <Text>{"\n"}</Text>
                    do you like?</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <View style={{backgroundColor: 'white', flex: 0.8, width: '99%', borderRadius: 10, padding: 10}}>
                    <Text style={{color:'#f2994a', fontSize: 30, textAlign: 'center', fontWeight: 'bold', paddingTop: 10, paddingBottom: 20}}>EUROPEAN</Text>
                    <Image source={require('./img/Steak.jpeg')} style={{width: '100%', height: '50%', borderRadius: 10}}/>
                    <Text>{"\n"}</Text>
                    <View>
                        <TouchableOpacity
                            style={{position: 'absolute', left: 0, paddingLeft: 10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/NO.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{position: 'absolute', right: 0, paddingRight:10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/YES.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

class FourthActivity extends Component {
    static navigationOptions =
        {
            title: 'Choose Food Category',
        };

    FunctionToOpenSecondActivity = () => {
        this.props.navigation.navigate('Fifth');

    };

    render()
    {
        return(
            <View style={styles.container2}>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_white, styles.h3, styles.boldText, styles.TextCenter]}>What categories
                    <Text>{"\n"}</Text>
                    do you like?</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <View style={{backgroundColor: 'white', flex: 0.8, width: '99%', borderRadius: 10, padding: 10}}>
                    <Text style={{color:'#f2994a', fontSize: 30, textAlign: 'center', fontWeight: 'bold', paddingTop: 10, paddingBottom: 20}}>FAST-FOOD</Text>
                    <Image source={require('./img/Burger.jpg')} style={{width: '100%', height: '50%', borderRadius: 10}}/>
                    <Text>{"\n"}</Text>
                    <View>
                        <TouchableOpacity
                            style={{position: 'absolute', left: 0, paddingLeft: 10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/NO.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{position: 'absolute', right: 0, paddingRight:10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/YES.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

class FifthActivity extends Component {
    static navigationOptions =
        {
            title: 'Choose Food Category',
        };

    FunctionToOpenSecondActivity = () => {
        this.props.navigation.navigate('Sixth');

    };

    render()
    {
        return(
            <View style={styles.container2}>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_white, styles.h3, styles.boldText, styles.TextCenter]}>What categories
                    <Text>{"\n"}</Text>
                    do you like?</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <View style={{backgroundColor: 'white', flex: 0.8, width: '99%', borderRadius: 10, padding: 10}}>
                    <Text style={{color:'#f2994a', fontSize: 30, textAlign: 'center', fontWeight: 'bold', paddingTop: 10, paddingBottom: 20}}>DIET-FOOD</Text>
                    <Image source={require('./img/diet.jpg')} style={{width: '100%', height: '50%', borderRadius: 10}}/>
                    <Text>{"\n"}</Text>
                    <View>
                        <TouchableOpacity
                            style={{position: 'absolute', left: 0, paddingLeft: 10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/NO.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{position: 'absolute', right: 0, paddingRight:10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Image source={require('./img/YES.png')} style={{width: 70, height: 70}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

class SixthActivity extends Component {
    static navigationOptions =
        {
            title: 'Choose Food Category',
        };
    FunctionToOpenSecondActivity = () => {
        this.props.navigation.navigate('Seventh');

    };
    render()
    {
        return(
            <View style={styles.container3}>
                <Text>{"\n"}</Text>
                <Text style={[styles.GreenTextStyle, styles.boldText, styles.TextCenter, styles.h2]}>COOK FOOD</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <Text style={[styles.GreenTextStyle]}>YOUR MENU {"\n"} IS EMPTY!</Text>
                <View style={{backgroundColor: 'white', flex: 0.8, width: '99%', borderRadius: 10, padding: 10}}>
                    <Text>{"\n"}</Text>
                    <Text>{"\n"}</Text>
                    <Text>{"\n"}</Text>
                    <Text>{"\n"}</Text>
                    <View>
                        <TouchableOpacity
                            style={{ backgroundColor:'#27ae60', padding: 10, borderRadius: 10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Text style={{textAlign:'center', color: '#ffffff', fontSize: 20}}>Menu by choice</Text>
                        </TouchableOpacity>
                        <Text>{"\n"}</Text>
                        <TouchableOpacity
                            style={{backgroundColor:'#27ae60', padding: 10, borderRadius: 10}}
                            onPress={this.FunctionToOpenSecondActivity}
                        >
                            <Text style={{textAlign:'center', color: '#ffffff', fontSize: 20}}>Menu by ingredients</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

class WeekMenuActivity extends Component {
    static navigationOptions =
        {
            title: 'Menu for week',
        };

    render()
    {
        return(
            <View style={{backgroundColor: 'white', flex: 1, alignItems: 'center'}}>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_green, styles.h3, styles.boldText, styles.TextCenter]}>THIS IS YOUR MENU
                    <Text>{"\n"}</Text>
                    FOR THIS WEEK</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <View style={{backgroundColor: '#27ae60', flex: 1, width: 300, borderRadius: 10, flexDirection: 'column', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Monday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food1</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Tuesday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food2</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Wednesday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food3</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Thursday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food4</Text>
                        </View>
                    </View>
                </View>
                <Text>{"\n"}</Text>
                <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                    <TouchableOpacity
                        onPress={this.FunctionToOpenSecondActivity}
                    >
                        <Image source={require('./img/NO.png')} style={{width: 70, height: 70, marginRight: 40}}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.FunctionToOpenSecondActivity}
                    >
                        <Image source={require('./img/YES.png')} style={{width: 70, height: 70}}/>
                    </TouchableOpacity>
                </View>
                <Text>{"\n"}</Text>
            </View>
        );
    }
}

export default Project = StackNavigator(
    {
        First: { screen: App },

        Second: { screen: SecondActivity },

        Third: { screen: ThirdActivity },

        Fourth: { screen: FourthActivity },

        Fifth: {screen: FifthActivity},

        Sixth: {screen: SixthActivity},

        Seventh: {screen: WeekMenuActivity}

    });


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#27ae60',
        justifyContent: 'center',
        paddingLeft: '10%',
        paddingRight: '10%',
    },
    container2: {
        flex: 1,
        backgroundColor: '#27ae60',
        justifyContent: 'center',
        paddingLeft: '10%',
        paddingRight: '10%',
    },
    container3: {
        flex: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        paddingLeft: '10%',
        paddingRight: '10%',
    },
    text_white: {
        color: '#ffffff',
    },
    text_green: {
        color: '#27ae60',
    },
    h1: {
        fontSize: 60,
    },
    h2: {
        fontSize: 50
    },
    h3: {
        fontSize: 35
    },
    h4: {
        fontSize: 25
    },
    boldText: {
        fontWeight: 'bold',
    },
    btnMainPage: {
        backgroundColor: '#ffffff',
        width: '99%',
        height: '12%',
        borderRadius: 12,
        alignItems: 'center',
    },
    btnTextStyle: {
        color: '#27ae60',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15
    },
    MainContainer:
    {
        justifyContent: 'center',
        flex:1,
        margin: 10

    },
    TextStyle:
    {
        fontSize: 23,
        textAlign: 'center',
        color: '#000',
    },
    GreenTextStyle:
        {
            fontSize: 23,
            textAlign: 'center',
            color: '#27ae60',
        },
    TextCenter:{
        textAlign: 'center',
    },
});

const weekMenuStyles = StyleSheet.create({
    foodImage: {
        backgroundColor: '#fff',
        width: 55,
        height: 55,
        borderRadius: 55,
        margin: 10
    },
    dayTitle: {
        color: '#ffffff',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'left',
    },
    foodTitle: {
        color: '#ffffff',
        fontSize: 15,
        textAlign: 'left',
    },
    menuText: {
        justifyContent: 'flex-start'
    }
});