
class WeekMenuActivity extends Component {
    static navigationOptions =
        {
            title: 'Choose Food Category',
        };

    render()
    {
        return(
            <View style={{backgroundColor: 'white', flex: 1, alignItems: 'center'}}>
                <Text>{"\n"}</Text>
                <Text style={[styles.text_green, styles.h3, styles.boldText, styles.TextCenter]}>THIS IS YOUR MENU
                    <Text>{"\n"}</Text>
                    FOR THIS WEEK</Text>
                <Text>{"\n"}</Text>
                <Text>{"\n"}</Text>
                <View style={{backgroundColor: '#27ae60', flex: 0.7, width: 300, borderRadius: 10}}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Monday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food1</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Tuesday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food2</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Wednesday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food3</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={weekMenuStyles.foodImage}></View>
                        <View style={weekMenuStyles.menuText}>
                            <Text style={weekMenuStyles.dayTitle}>Thursday</Text>
                            <Text style={weekMenuStyles.foodTitle}>Food4</Text>
                        </View>
                    </View>
                </View>
                <View >
                    <TouchableOpacity
                        style={{position: 'absolute', left: 0}}
                        onPress={this.FunctionToOpenSecondActivity}
                    >
                        <Image source={require('./img/dislike.png')} style={{width: 80, height: 80, margin: 30}}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{position: 'absolute', right: 0}}
                        onPress={this.FunctionToOpenSecondActivity}
                    >
                        <Image source={require('./img/like.png')} style={{width: 70, height: 70, margin: 30}}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const weekMenuStyles = StyleSheet.create({
    foodImage: {
        backgroundColor: '#fff',
        width: 55,
        height: 55,
        borderRadius: 55,
        margin: 10
    },
    dayTitle: {
        color: '#ffffff',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'left',
    },
    foodTitle: {
        color: '#ffffff',
        fontSize: 15,
        textAlign: 'left',
    },
    menuText: {
        justifyContent: 'flex-start'
    }
});