class EmptyFridgeActivity extends Component {
    static navigationOptions =
        {
            title: 'Choose Food Category',
        };

    render()
    {
        return(
            <View style={{backgroundColor: 'white', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View style={{backgroundColor: 'white', flex: 0.85, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={[styles.text_green, styles.h3, styles.TextCenter]}>YOUR FRIDGE
                        <Text>{"\n"}</Text>
                        IS EMPTY!</Text>
                </View>
                <View style={{backgroundColor: 'white', flex: 0.15, alignItems: 'center', justifyContent: 'center'}}>
                    <TouchableOpacity onPress={this.OpenWeekMenuActivity}>
                        <Image source={require('./img/dislike.png')} style={{width: 80, height: 80}}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}